#!/bin/sh
export ARCH=arm
export HOST=arm-linux-gnueabihf
export CROSS_COMPILE=arm-linux-gnueabihf-
SDK_PATH=/home/ws/sbc-imx6q/imx6q-sbc/
export PATH=$PATH:${SDK_PATH}/tools/cross-compiler/arm-linux-gnueabihf-4.9.1/bin
