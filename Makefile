SDK_PATH = /home/ws/sbc-imx6q/imx6q-sbc/
KERN_DIR = ${SDK_PATH}/boot/linux-3.14.28/

all:
	make -C $(KERN_DIR) M=`pwd` modules
clean:
	make -C $(KERN_DIR) M=`pwd` modules clean
	rm -rf modules.order
    
obj-m   := sja_can.o
sja_can-objs := sja1000.o sja1000_platform.o

